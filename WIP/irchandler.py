# IRC handler for Pesterdumb bot. Use this to handle the IRC-y bit.
import asyncore, datetime, socket, sys

class IrcHandlerAsync(asyncore.dispatcher):
	dh = None
	loggedIn = False
	inChannel = False
	moduleList = {}
	def __init__(self, dh, ch, sh):
		self.moduleList = {"DH": dh, "CH":ch, "SH":sh}
		asyncore.dispatcher.__init__(self)
		self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
		self.connect( (self.moduleList["DH"].data["Server"], self.moduleList["DH"].data["Port"]) )
		print "IHA: Connecting..."
		self.buffer = ""
		# messages = self.recv(4096)
		# self.buffer = 'GET %s HTTP/1.0\r\n\r\n' % path

	def handle_connect(self):
		pass
		
	def handle_close(self):
		self.close()
		
	def handle_read(self):
		data = self.recv(8192)
		if data.find("PING") != -1:
			self.send("PONG" + data.split()[1])
		if not self.loggedIn:
			self.send("User " + self.moduleList["DH"].data["Nick"] + " " + self.moduleList["DH"].data["Nick"] + " " + self.moduleList["DH"].data["Nick"] + " :" + self.moduleList["DH"].data["Nick"] + " IRC\r\n")
			self.send("Nick " + self.moduleList["DH"].data["Nick"] + "\r\n")
		elif not self.inChannel:
			self.irc.send("PRIVMSG nickServ :IDENTIFY " + self.moduleList["DH"].data["Password"] + "\r\n")
			self.irc.send("JOIN " + self.moduleList["DH"].data["Channel"] + "\r\n")
		if len(sys.argv) > 1:
			if sys.argv[1] == "--debug":
				print "CH-D: Raw data: " + messages
		parsed = self.moduleList["SH"].parse()
		if len(sys.argv) > 1:
			if sys.argv[1] == "--debug":
				self.listOfMessages.append(messages)
		else:
			if parsed["Purpose"][0] != "ping" and parsed["Purpose"][0] != "PESTERCHUMTIME":
				self.listOfMessages.append(parsed["User"] + ": " + parsed["Message"] + " " + str(parsed["Purpose"]) + "\r\n")
		if parsed["Purpose"][0] == "ping":
			print "IH: Pong."
			self.irc.send("PONG " + parsed["Message"] + "\r\n")
		elif parsed["Purpose"][0] == "left":
			print "IH: ", parsed["User"], parsed["Message"]
		elif parsed["Purpose"][0] == "join":
			self.irc.send("PRIVMSG " + self.moduleList["DH"].data["Channel"] + " :PESTERCHUM:TIME>i \r\n")
			print "IH: ", parsed["User"], parsed["Message"]
		elif parsed["Purpose"][0] == "banhonk":
			print "IH: Banned " , parsed["User"], " for honking."
			self.irc.send("KICK " + self.moduleList["DH"].data["Channel"] + " " + parsed["User"] + " :Unauthorized clown sounds!\r\n") 
		elif parsed["Purpose"][0] == "bansmilies":
			print "IH: Banned ", parsed["User"], " for smilies."
			self.irc.send("KICK " + self.moduleList["DH"].data["Channel"] + " " + parsed["User"] + " :Unauthorized smilies!\r\n") 
		elif parsed["Purpose"][0] == "invalidcommand":
			print "IH: Got an invalid command."
		elif parsed["Purpose"][0] == "invalidinternal":
			print "IH: Got an invalid internal message."
		else:
			self.run = self.moduleList["CH"].execute(dh, self, sh, parsed)	
		if len(sys.argv) > 1:
			if sys.argv[1] == "--debug":
				results = data
		else:
			if parsed["Purpose"][0] != "ping" and parsed["Purpose"][0] != "PESTERCHUMTIME":
				results = parsed["User"] + ": " + parsed["Message"] + " " + str(parsed["Purpose"]) + "\r\n"
			else:
				results.append("")
		return results

	def writable(self):
		return (len(self.buffer) > 0)
			
class IrcHandler:
	irc = False
	listOfMessages = []
	firsttime = True
	run = True
	def __init__(self, dh, ch, sh):
		self.irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.irc.connect((dh.data["Server"], dh.data["Port"]))
		print "IH: Connecting..."
		messages = self.irc.recv(4096)
		if messages.find("PING") != -1:
			self.irc.send("PONG " + messages.split()[1])
		self.irc.send("User " + dh.data["Nick"] + " " + dh.data["Nick"] + " " + dh.data["Nick"] + " :" + dh.data["Nick"] + " IRC\r\n")
		self.irc.send("Nick " + dh.data["Nick"] + "\r\n")
		if len(sys.argv) > 1:
			if sys.argv[1] == "--debug":
				print "CH-D: Raw data: " + messages
		print "IH: Connected."
	def main(self, dh, ch, sh):
		messages = self.irc.recv(4096)
		if self.firsttime:
			self.irc.send("PRIVMSG nickServ :IDENTIFY " + dh.data["Password"] + "\r\n")
			self.irc.send("JOIN " + dh.data["Channel"] + "\r\n")
			print "IH: Identified and joined channel."
			self.firsttime = False
