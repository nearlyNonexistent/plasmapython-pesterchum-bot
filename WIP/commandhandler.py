# Command handler for Pesterdumb bot.
import socket, asyncore
import datetime
import pytz
import urllib2
from xml.sax import saxutils
import re
import random

class HtmlHandlerAsync(asyncore.dispatcher):

	def __init__(self, host, path):
		asyncore.dispatcher.__init__(self)
		self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
		self.connect( (host, 80) )
		self.buffer = 'GET %s HTTP/1.0\r\n\r\n' % path

	def handle_connect(self):
		pass
		
	def handle_close(self):
		self.close()
		
	def handle_read(self):
		print self.recv(8192)
			
	def writable(self):
		return (len(self.buffer) > 0)
	
	def handle_write(self):
		sent = self.send(self.buffer)
		self.buffer = self.buffer[sent:]

class CommandHandler:
	MESSAGER = ""
	timer = None
	def __init__(self, dh):
		self.MESSAGER = "PRIVMSG " + dh.data["Channel"] + " :<c=#" + dh.data["Color"] + ">"+dh.data["Initials"]+": "
		self.timer =  datetime.datetime.now()
	def execute(self, dh, ih, sh, parsed):
		if parsed["Purpose"][0] == "time":
			time = pytz.utc.localize(datetime.datetime.utcnow())
			if len(parsed["Message"].split()) > 1:
				if parsed["Message"].split()[1] in pytz.all_timezones:
					time = time.astimezone(pytz.timezone(parsed["Message"].split()[1]))
				else:
					time = "Not a valid timezone"
			else:
				time = time.astimezone(pytz.timezone("US/Central"))
			if time != "Not a valid timezone":
				time = time.strftime("%Y-%m-%d %H:%M %Z")
			ih.irc.send(self.MESSAGER + time + ".\r\n")
			print "CH: Gave system time to ", parsed["User"]
		elif parsed["Purpose"][0] == "help":
			if len(parsed["Message"].split()) > 1:
				msg = sh.commandParse(parsed["Message"].split()[1])
				ih.irc.send(self.MESSAGER + "~" + msg[0] + ": " + msg[1] + "\r\n")
			else:
				msg = sh.commandParse("ALL_KEYS")
				stre = ""
				for iter in msg:
					stre = stre + "~" + iter + " "
				ih.irc.send(self.MESSAGER + "List of commands: " + stre + "\r\n")
		elif parsed["Purpose"][0] == "version":
			print "CH: Gave version to ", parsed["User"]
			ih.irc.send(self.MESSAGER + str(dh.data["Version"]) + " \r\n")
		elif parsed["Purpose"][0] == "ban":
			if parsed["User"] == dh.data["Mod"]:
				if len(parsed["Message"].split()) > 1:
					whotoban = parsed["Message"].split()[1]
					ih.irc.send("KICK " + dh.data["Channel"] + " " + whotoban + " :Requested by memo creator.\r\n")
				else:
					ih.irc.send(self.MESSAGER + "You didn't tell me who to ban. \r\n")
			else:
				ih.irc.send("KICK " + dh.data["Channel"] + " " + parsed["User"] + " :Unauthorized attempt to use the ban command.\r\n") 
		elif parsed["Purpose"][0] == "quit":
			if parsed["User"] == dh.data["Mod"]:
				ih.irc.send("QUIT :Requested by mod.")
				print "Shutdown by mod."
				dh.saveLog(ih.listOfMessages, "bot.log")
				ih.irc.shutdown(socket.SHUT_RDWR)
				ih.irc.close()
				return 0
		elif parsed["Purpose"][0] == "wtf":
			secs = datetime.datetime.now() - self.timer
			if secs.seconds > 5:
				if len(parsed["Message"].split()) > 1:
					x = len(parsed["Message"].split())
					query = parsed["Message"].split()[1]
					if len(parsed["Message"].split()) > 1:
						for iter in parsed["Message"].split()[2:x]:
							query = query + " " + iter
					print query
					response = urllib2.urlopen("https://simple.wikipedia.org/w/api.php?action=parse&prop=text&format=xml&page="+urllib2.quote(query))
					html = response.read()
					html = saxutils.unescape(html)
					messages = re.search("<p>(.+?)</p>", html)
					if messages != None:
						message = messages.group(0)
						message = re.sub(r"<.+?>", "", message)
						message = re.sub(r"&.+?;", "", message)
					else:
						message = "Page does not exist."
					ih.irc.send(self.MESSAGER + message + "\r\n")
					self.timer = datetime.datetime.now()
					if messages != None:
						ih.irc.send(self.MESSAGER + "More at: " + "https://simple.wikipedia.org/wiki/" + urllib2.quote(query) + "\r\n")
				else:
					ih.irc.send(self.MESSAGER + "You didn't give me a thing to lookup.\r\n")
			else:
				ih.irc.send(self.MESSAGER + "Cooldown. Wait 5 seconds to search again.\r\n")
		elif parsed["Purpose"][0] == "log":
			secs = datetime.datetime.now() - self.timer
			if secs.seconds > 5:
				dh.saveLog(ih.listOfMessages, "bot.log")
				e = dh.exportLog("bot.log")
				ih.irc.send(self.MESSAGER + e + "\r\n")
				self.timer = datetime.datetime.now()
			else:
				ih.irc.send(self.MESSAGER + "Cooldown. Wait 5 seconds to log again.\r\n")
		elif parsed["Purpose"][0] == "rtd":
			random.seed()
			if len(parsed["Message"].split()) > 1:
				digitsToRoll = re.findall("(\d+)d(\d+)", parsed["Message"].split()[1])
				print str(digitsToRoll)
				if len(digitsToRoll) > 0:
					amountToRoll = int(digitsToRoll[0][0])
					sides = int(digitsToRoll[0][1])
					if amountToRoll > 20 or sides > 100:
						ih.irc.send(self.MESSAGER + "I am not allowed to roll a dice with more sides than 100, or more dice than 20 at a time.\r\n")
					else:
						results = []
						for x in range(0, amountToRoll):
							results.append(random.randint(1, sides))
						ih.irc.send(self.MESSAGER + str(results) + " = " + str(sum(results)) + "\r\n")
				else:
					ih.irc.send(self.MESSAGER + "Invalid input.\r\n")
			else:
				roll = random.randint(1, 6)
				ih.irc.send(self.MESSAGER + str(roll) + "\r\n")
		elif parsed["Purpose"][0] == "isup":
			if len(parsed["Message"].split()) > 1:
				address = parsed["Message"].split()[1]
				response = urllib2.urlopen("http://www.isup.me/"+address)
				html = response.read().lower()
				if html.find("it's just you.") != -1:
					ih.irc.send(self.MESSAGER + "It's just you.\r\n")
				else:
					ih.irc.send(self.MESSAGER + "It's down.\r\n")
				print "CH: Checked if website is down."
			else:
				ih.irc.send(self.MESSAGER + "You didn't give me a site to check.\r\n")
		return 1