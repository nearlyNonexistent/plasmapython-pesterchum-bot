import datahandler
import speechhandler
import irchandler
import commandhandler
from Tkinter import *
import tkMessageBox
from msvcrt import getch
import asyncore

class Application(Frame):
	moduleList = {}
	def __init__(self, master=None, modules={"error"}):
		Frame.__init__(self, master)
		self.pack()
		self.moduleList = modules
		self.createWidgets()
		self.master.wm_title("Pesterdumb")
		self.master.after(1, self.runloop)
		self.moduleList["IH"].connect()
	def runloop(self):
		data = self.moduleList["IH"].recv(8192)
		print "PD: " + str(data)
		self.TEXT.insert(END, data)
		self.master.after(1, self.runloop)
	def createWidgets(self):
		self.SCROLLBAR = Scrollbar(self)
		self.SCROLLBAR.pack(side=RIGHT, fill=Y)
		self.TEXT = Text(self, width=50, height=10, yscrollcommand = self.SCROLLBAR.set)
		self.TEXT.pack(side=LEFT, fill=BOTH)
		self.SCROLLBAR.config(command=self.TEXT.yview)

root = Tk()

dh = datahandler.DataHandler("data.cfg")
sh = speechhandler.SpeechHandler()
ch = commandhandler.CommandHandler(dh)
# ih = irchandler.IrcHandler(dh, ch, sh)
ih = irchandler.IrcHandlerAsync(dh, ch, sh)

moduleList = {"DH":dh, "SH":sh, "CH":ch, "IH":ih}
def handler():
	if tkMessageBox.askokcancel("Quit?", "Are you sure you want to quit?"):
		root.destroy()
app = Application(modules=moduleList, master=root)
root.protocol("WM_DELETE_WINDOW", handler)
app.mainloop()

print "Hit any key to continue..."
getch()