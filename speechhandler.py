# Speech handler for Pesterdumb bot. Use this to parse messages.
import string
import re
import sys
class SpeechHandler:
	def parse(self, text):
		dict = {"User":"none", "Message":"none", "Purpose":["none", "This wasn't defined."]}
		if text.find("!") != -1:
			dict["User"] = text.split("!")[0][1:]
		else:
			dict["User"] = text.split()[0]
		if len(text.split()) > 1:
			commandType = self.internalParse(text.split()[1])
		else:
			commandType = "???"
		dict["Purpose"] = commandType
		if text.split()[0] == "PING":
			dict["Message"] = text.split(":")[1]
			dict["Purpose"] = ["ping", "The network asked if I was still connected."]
		elif commandType[0] == "part":
			dict["Message"] = " ceased responding to memo."
		elif commandType[0] == "join":
			dict["Message"] = " joined the memo."
		elif commandType[0] == "message":
			if text.find("PESTERCHUM:TIME") != -1:
				purpose = ["PESTERCHUMTIME", "They gave their timestamp."]
				message = "none"
			else:
				# First we check if it's a /me or a normal message:
				mesg = text.split()[3]
				textlen = len(text.split(":"))
				if mesg.find("/me") != -1:
					sliceat = slice(2, textlen)
					purpose = ["slashme", "They did a /me command"]
				else:
					sliceat = slice(2, textlen)
					purpose = ["message", "They did a normal Pesterchum message."]
				# Then we fuse it down in case the message contained colons, creating our message variable:
				iteralist = text.split(":")[sliceat]
				message = ""
				for iter in iteralist:
					message = message + iter
				message = re.sub(r"<c=\S*?>", "", message) # Removing color tags
				message = message.replace("</c>", "") # Removing end color tags
				message = message.replace("\r\n", "") # Removing linebreaks
				if message.find("~isup") == -1 and message.find("~time") == -1 and message.find("~wtf") == -1:
					message = message.translate(string.maketrans("",""), "!\"#;$%^&*()'_-`?/\\<>,.{}[]|") # Removing punctuation
				if len(message) > 0: # If the message length is above one, do this stuff:
					if purpose[0] == "slashme":
						message = message[3:]
					else:
						message = message[3:]
				# Check for bannable offenses:
				if text.lower().find("honk") != -1:
					purpose = ["banhonk", "They used an unauthorized soundbyte."]
				elif text.lower().find(":whatdidyoudo:") != -1 or text.lower().find(":billiards:") != -1 or text.find(":billiardslarge:") != -1:
					purpose = ["bansmilies", "They used an unauthorized smilie."]
				# Make sure it's not a command, which takes precedence:
				if len(message) > 0:
					if message[0] == "~":
						print "SH-D: Message is command."
						if self.commandParse(message.split()[0])[0] != "invalidcommand":
							print "SH-D: Message is not invalid command."
							purpose = self.commandParse(message.split()[0][1:])
						else:
							print "SH-D: Message is invalid command."
							if purpose != "banhonk" and purpose != "bansmilies":
								purpose = self.commandParse(message.split()[0][1:])
			dict["Message"] = message
			dict["Purpose"] = purpose
		if len(sys.argv) > 1:
			if sys.argv[1] == "--debug":
				print "SH-D: Raw data: " + text
		print "SH: Parsed as: ", ', '.join("{!s}={!r}".format(key,val) for (key,val) in dict.items())
		return dict
	def commandParse(self, message):
		returnmsg = {
		'ban': ['ban', 'Type "~ban [user]" to ban them. Only for NN.'],
		'time': ['time', 'Type "~time [zone]" to check the time for that zone. Examples: US/Pacific, US/Central, GMT, MST...'],
		'version': ['version', 'Type "~version" to get my version number.'],
		'isup': ['isup', 'Type "~isup [website]" to check if it\'s down.'],
		'quit': ['quit', 'Type "~quit" to exit me. Only for NN.'],
		'help': ['help', 'Type "~help [command]" for info on it, or no command for a list.'],
		'wtf': ['wtf', 'Type "~wtf [thing]" to search Wikipedia for it.'],
		'log': ['log', 'Type "~log" to get a Pastebin of my log.'],
		'rtd': ['rtd', 'Type "~rtd" to get a dice roll.']
		}
		if not message == "ALL_KEYS":
			return returnmsg.get(message.lower(), ["invalidcommand", "That isn't a real command."])
		else:
			return returnmsg.keys()
	def internalParse(self, message):
		return {
		'privmsg': ['message', 'They sent a message.'],
		'part': ['left', 'They left the memo.'],
		'join': ['join', 'They joined the memo.']
		}.get(message.lower(), ["invalidinternal", "Invalid internal message."])