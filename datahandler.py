# Data handler module for Pesterdumb bot. Use this for handling the loading and saving of the data. 
import sys
import os.path
import pickle
import urllib2
import urllib

class DataHandler:
	_DEFAULT = {"Port":6667,"Server":"irc.mindfang.org", "Channel":"#SBARGv2_Debug", "Nick":"plasmaPython", "Password":"bluh", "Color":"44036F", "Initials":"PP", "Version":'1.9.4: Switched to main Wikipedia rather than simple.'}
	data = {}
	def __init__(self, file):
		if len(sys.argv) > 1:
			if sys.argv[1] == "--debug":
				print "DH-D: Running in debug mode."
		if not os.path.exists(file):
			self._saveData(file)
			self._loadData(file)
		else:
			self._loadData(file)
		print "DH: Data loaded."
	def _saveData(self, file):
		try:
			print "DH: Creating default settings file..."
			fh = open(file, 'w')
			pickle.dump(self._DEFAULT, fh)
		except IOError as e:
			print "DH: ", "({})".format(e)
	def _loadData(self, file):
		try:
			print "DH: Loading settings..."
			fh = open(file, 'r')
			self.data = pickle.load(fh)
			fh.close()
		except IOError as e:
			print "DH: ", "({})".format(e)
	def saveLog(self, data, file):
		try:
			print "DH: Saving log file..."
			fh = open(file, 'w')
			for cur in data:
				fh.write(str(cur) + "\n")
			fh.close()
		except IOError as e:
			print "DH: ", "({})".format(e)
	def exportLog(self, file):
		fh = open(file, 'r')
		paste = fh.read()
		fh.close()
		sh = urllib2.urlopen("http://pastebin.com/api/api_post.php",urllib.urlencode({"api_option":"paste","api_dev_key": self.data["Devkey"], "api_paste_code": paste}))
		link = sh.read()
		print link
		return link