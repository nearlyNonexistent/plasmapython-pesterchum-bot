# Chat handler for Pesterdumb bot. Use this to handle the IRC-y bit.
import socket
import time, datetime, pytz
import sys
import urllib2
import re

class ChatHandler:
	irc = False
	MESSAGER = ""
	listOfMessages = []
	def __init__(self, dh, sh):
		self.irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.irc.connect((dh.data["Server"], dh.data["Port"]))
		print "CH: Connecting..."
		messages = self.irc.recv(4096)
		if messages.find("PING") != -1:
			self.irc.send("PONG " + messages.split()[1])
		self.irc.send("User " + dh.data["Nick"] + " " + dh.data["Nick"] + " " + dh.data["Nick"] + " :" + dh.data["Nick"] + " IRC\r\n")
		self.irc.send("Nick " + dh.data["Nick"] + "\r\n")
		if len(sys.argv) > 1:
			if sys.argv[1] == "--debug":
				print "CH-D: Raw dh.data: " + messages
		print "CH: Connected."
		self.MESSAGER = "PRIVMSG " + dh.data["Channel"] + " :<c=#" + dh.data["Color"] + ">"+dh.data["Initials"]+": "
		self._main(dh, sh)
	def _main(self, dh, sh):
		firsttime = True
		while True:
			messages = self.irc.recv(4096)
			parsed = sh.parse(messages)
			if len(sys.argv) > 1:
				if sys.argv[1] == "--debug":
					self.listOfMessages.append(messages)
			else:
				self.listOfMessages.append(parsed)
			if parsed["Purpose"] == "ping":
				print "CH: Pong."
				self.irc.send("PONG " + parsed["Message"] + "\r\n")
			elif parsed["Purpose"] == "left":
				print "CH: ", parsed["User"], parsed["Message"]
			elif parsed["Purpose"] == "join":
				self.irc.send("PRIVMSG " + dh.data["Channel"] + " :PESTERCHUM:TIME>i \r\n")
				print "CH: ", parsed["User"], parsed["Message"]
			elif parsed["Purpose"] == "banhonk":
				print "CH: Banned " , parsed["User"], " for honking."
				self.irc.send("KICK " + dh.data["Channel"] + " " + parsed["User"] + " :Unauthorized clown sounds!\r\n") 
			elif parsed["Purpose"] == "bansmilies":
				print "CH: Banned ", parsed["User"], " for smilies."
				self.irc.send("KICK " + dh.data["Channel"] + " " + parsed["User"] + " :Unauthorized smilies!\r\n") 
			elif parsed["Purpose"] == "time":
				if len(sys.argv) > 1:
					if sys.argv[1] == "--debug":
						print "CH-D: Raw dh.data being sent: " + self.MESSAGER + time.strftime("%H:%M") + "\r\n"
				self.irc.send(self.MESSAGER + time.strftime("%H:%M") + " CST.\r\n")
				print "CH: Gave system time to ", parsed["User"]
			elif parsed["Purpose"] == "version":
				print "CH: Gave version to ", parsed["User"]
				self.irc.send(self.MESSAGER + str(dh.data["Version"]) + " \r\n")
			elif parsed["Purpose"] == "ban":
				if parsed["User"] == dh.data["Mod"]:
					if len(parsed["Message"].split()) > 1:
						whotoban = parsed["Message"].split()[1]
						self.irc.send("KICK " + dh.data["Channel"] + " " + whotoban + " :Requested by memo creator.\r\n")
					else:
						self.irc.send(self.MESSAGER + "You didn't tell me who to ban. \r\n")
				else:
					self.irc.send("KICK " + dh.data["Channel"] + " " + parsed["User"] + " :Unauthorized attempt to use the ban command.\r\n") 
			elif parsed["Purpose"] == "quit":
				if parsed["User"] == dh.data["Mod"]:
					self.irc.send("QUIT :Requested by mod.")
					print "Shutdown by mod."
					dh.saveLog(self.listOfMessages, "bot.log")
					self.irc.shutdown(socket.SHUT_RDWR)
					self.irc.close()
					break
			elif parsed["Purpose"] == "isup":
				if len(parsed["Message"].split()) > 1:
					address = parsed["Message"].split()[1]
					response = urllib2.urlopen("http://www.isup.me/"+address)
					html = response.read().lower()
					if html.find("it's just you.") != -1:
						self.irc.send(self.MESSAGER + "It's just you.\r\n")
					else:
						self.irc.send(self.MESSAGER + "It's down.\r\n")
					print "CH: Checked if website is down."
				else:
					self.irc.send(self.MESSAGER + "You didn't give me a site to check.\r\n")
			elif parsed["Purpose"] == "invalidcommand":
				print "CH: user attempted invalid command: ", parsed["Message"]
			if firsttime:
				self.irc.send("PRIVMSG nickServ :IDENTIFY " + dh.data["Password"] + "\r\n")
				self.irc.send("JOIN " + dh.data["Channel"] + "\r\n")
				time = pytz.utc.localize(datetime.datetime.utcnow())
				time = time.astimezone(pytz.timezone("US/Central"))
				time = time.strftime("%H")
				print ("TIME:"+time)
				self.irc.send(self.MESSAGER + "Good morning, you all! It's " + time + " in Texas! Time to wake up! Let's all strive to do our best today!\r\n")
				print "CH: Identified and joined channel."
				firsttime = False