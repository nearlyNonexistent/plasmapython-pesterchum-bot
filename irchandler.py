# IRC handler for Pesterdumb bot. Use this to handle the IRC-y bit.
import socket
import datetime
import sys

class IrcHandler:
	irc = False
	listOfMessages = []
	def __init__(self, dh, ch, sh):
		self.irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.irc.connect((dh.data["Server"], dh.data["Port"]))
		print "IH: Connecting..."
		messages = self.irc.recv(4096)
		if messages.find("PING") != -1:
			self.irc.send("PONG " + messages.split()[1])
		self.irc.send("User " + dh.data["Nick"] + " " + dh.data["Nick"] + " " + dh.data["Nick"] + " :" + dh.data["Nick"] + " IRC\r\n")
		self.irc.send("Nick " + dh.data["Nick"] + "\r\n")
		if len(sys.argv) > 1:
			if sys.argv[1] == "--debug":
				print "CH-D: Raw dh.data: " + messages
		print "IH: Connected."
		self._main(dh, ch, sh)
	def _main(self, dh, ch, sh):
		firsttime = True
		run = True
		while run == True:
			messages = self.irc.recv(4096)
			parsed = sh.parse(messages)
			if len(sys.argv) > 1:
				if sys.argv[1] == "--debug":
					self.listOfMessages.append(messages)
			else:
				if parsed["Purpose"][0] != "ping" and parsed["Purpose"][0] != "PESTERCHUMTIME":
					self.listOfMessages.append(parsed["User"] + ": " + parsed["Message"] + " " + str(parsed["Purpose"]) + "\r\n")
			if parsed["Purpose"][0] == "ping":
				print "IH: Pong."
				self.irc.send("PONG " + parsed["Message"] + "\r\n")
			elif parsed["Purpose"][0] == "left":
				print "IH: ", parsed["User"], parsed["Message"]
			elif parsed["Purpose"][0] == "join":
				self.irc.send("PRIVMSG " + dh.data["Channel"] + " :PESTERCHUM:TIME>i \r\n")
				print "IH: ", parsed["User"], parsed["Message"]
			elif parsed["Purpose"][0] == "banhonk":
				print "IH: Banned " , parsed["User"], " for honking."
				self.irc.send("KICK " + dh.data["Channel"] + " " + parsed["User"] + " :Unauthorized clown sounds!\r\n") 
			elif parsed["Purpose"][0] == "bansmilies":
				print "IH: Banned ", parsed["User"], " for smilies."
				self.irc.send("KICK " + dh.data["Channel"] + " " + parsed["User"] + " :Unauthorized smilies!\r\n") 
			elif parsed["Purpose"][0] == "invalidcommand":
				print "IH: Got an invalid command."
			elif parsed["Purpose"][0] == "invalidinternal":
				print "IH: Got an invalid internal message."
			else:
				run = ch.execute(dh, self, sh, parsed)
			if firsttime:
				self.irc.send("PRIVMSG nickServ :IDENTIFY " + dh.data["Password"] + "\r\n")
				self.irc.send("JOIN " + dh.data["Channel"] + "\r\n")
				print "IH: Identified and joined channel."
				firsttime = False