import datahandler
import speechhandler
import irchandler
import commandhandler

dh = datahandler.DataHandler("data.cfg")
sh = speechhandler.SpeechHandler()
ch = commandhandler.CommandHandler(dh)
ih = irchandler.IrcHandler(dh, ch, sh)